The ROC results for this dataset are:

TP:			7
FP:			23
Precision:		23.33%
Recall:			50.00%
Structural Accuracy:	62.96%
Similarity:		39.15%
