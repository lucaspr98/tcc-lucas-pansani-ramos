/**
	main.cpp

    Purpose: Calls Inference class in order to generate the inferred network and calls Validation class in order to	calculate the generated network with the real network. 

    Author: Lucas Pansani Ramos
*/

#include "Inference.h"
#include "Validation.h"
#include <chrono>

using namespace std;

int main(int argc, char *argv[])
{
	ifstream indata, realNetwork;

	string dataFileName = "";
	dataFileName.append(argv[1]);
	dataFileName.append(argv[2]);

	string realNetworkFileName = "";
	realNetworkFileName.append(argv[1]);
	realNetworkFileName.append(argv[3]);
	
	//Open file with the dataset
	indata.open(dataFileName, ifstream::in);

	//Open file with the real network
	realNetwork.open(realNetworkFileName, ifstream::in);

	//represents the number of most informative genes wanted
	int k = strtol(argv[4], NULL, 10);

	//start to measure inference runtime
	auto start = chrono::high_resolution_clock::now();

	//Initialize an Inference object, which will generate the inferred network
	Inference *I = new Inference(indata, k);
	
	//stop measuring inference runtime
	auto finish = chrono::high_resolution_clock::now();

	chrono::duration<double> elapsed = finish - start;

	cout << elapsed.count() << endl;

	string resultsPath = "results/";
	resultsPath.append(argv[1]);
	resultsPath.erase(resultsPath.begin() + 8, resultsPath.begin()+ 13);
	resultsPath.append(argv[2]);
	resultsPath.erase(resultsPath.size()-4, resultsPath.size());


	//Validates the inferred network with the real network
	Validation *V = new Validation(realNetwork, I->getInferredNetwork(), I->getNumberOfEdges(), I->getNumberOfGenes(), I->getGeneWasInferred(), resultsPath);

	// //Close file with the data
	indata.close();

	// //Close file with real network
	realNetwork.close();

	return 0;
}