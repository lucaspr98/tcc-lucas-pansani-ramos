The ROC results for this dataset are:

TP:			12
FP:			102
Precision:		10.53%
Recall:			6.82%
Structural Accuracy:	97.29%
Similarity:		32.27%
