The ROC results for this dataset are:

TP:			12
FP:			114
Precision:		9.52%
Recall:			5.69%
Structural Accuracy:	96.81%
Similarity:		30.68%
