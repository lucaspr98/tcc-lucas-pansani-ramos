The ROC results for this dataset are:

TP:			5
FP:			25
Precision:		16.67%
Recall:			45.45%
Structural Accuracy:	61.73%
Similarity:		32.73%
