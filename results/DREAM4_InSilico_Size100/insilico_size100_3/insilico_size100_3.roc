The ROC results for this dataset are:

TP:			10
FP:			95
Precision:		9.52%
Recall:			5.15%
Structural Accuracy:	97.15%
Similarity:		30.71%
