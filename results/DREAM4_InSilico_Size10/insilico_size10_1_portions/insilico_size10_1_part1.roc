The ROC results for this dataset are:

TP:			2
FP:			21
Precision:		8.70%
Recall:			14.29%
Structural Accuracy:	59.26%
Similarity:		24.43%
