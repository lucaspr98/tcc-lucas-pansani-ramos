The ROC results for this dataset are:

TP:			9
FP:			24
Precision:		27.27%
Recall:			32.14%
Structural Accuracy:	57.43%
Similarity:		42.79%
