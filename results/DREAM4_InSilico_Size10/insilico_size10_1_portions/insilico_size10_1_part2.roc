The ROC results for this dataset are:

TP:			5
FP:			19
Precision:		20.83%
Recall:			35.71%
Structural Accuracy:	65.43%
Similarity:		38.63%
