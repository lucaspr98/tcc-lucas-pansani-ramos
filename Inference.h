/**
	  Inference.h
                            
    Purpose: Uses the data passed by parameter to infer the network. This inference occurs by calling the methods MIFS and SWAP

    Author: Lucas Pansani Ramos
*/
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <string>
#include "Gene.h"

using namespace std;

class Inference
{
public:
  Inference(ifstream & data, int k)
  {
    int i;

    this->kValue = k;

    //Read number of genes and number of samples
    data >> this->numberOfGenes >> this->numberOfSamples;

    //Create genes, add its samples values and computes its entropy
    createGenes(data);

    //Allocate jointEntropyValues and mutualInformationValues matrixes
    this->jointEntropyValues = new double *[numberOfGenes];
    this->mutualInformationValues = new double *[numberOfGenes];

    for (i = 0; i < this->numberOfGenes; i++)
    {
      this->jointEntropyValues[i] = new double[numberOfGenes];
      this->mutualInformationValues[i] = new double[numberOfGenes];
    }

    //Fill jointEntropy and mutualInformationValues matrixes
    computesJointEntropies();
    computesMutualInformations();    

    this->runMIFS();

    this->runSWAP();

    this->fillInferredNetwork();

    for(i = 0; i < this->numberOfGenes; i++){
      if(this->genes[i]->mostInformativeGenesSize())
        this->geneWasInferred.push_back(1);
      else
        this->geneWasInferred.push_back(0);
      
    }

  }

  ~Inference()
  {
    int i;
    for (i = 0; i < this->numberOfGenes; i++)
    {
      delete[] this->jointEntropyValues[i];
      delete[] this->mutualInformationValues[i];
      delete[] this->MIFSmatrix[i];
      delete[] this->SWAPmatrix[i];
      delete[] this->remainingGenes[i];
    }

    for (i = 0; i < this->numberOfSamples; i++)
    {
      delete[] this->inferredNetwork[i];
    }

    delete[] this->jointEntropyValues;
    delete[] this->mutualInformationValues;
    delete[] this->inferredNetwork;
    delete[] this->MIFSmatrix;
    delete[] this->SWAPmatrix;
    delete[] this->remainingGenes;
  }

  /**
		Prints joint entropies matrix
	*/
  void printJointEntropies();

  /**
		Prints mutual information matrix
	*/
  void printMutualInformations();

  /**
		Prints mifs matrix
	*/
  void printMIFSmatrix();

  /**
		Prints swap matrix
	*/
  void printSWAPmatrix();

  /**
		Prints remaining genes matrix
	*/
  void printRemainingGenes();

  /**
		Prints most informative genes mutual information with the target (for all genes)
	*/
  void printMostInformativeMI();

  /**
		Return the inferred network matrix
	*/
  int **getInferredNetwork();

  /**
		Return the number of edges from a complete graph with numberOfGenes vertexes
	*/
  int getNumberOfEdges();

  /**
		Return the number of genes on this network
	*/
  int getNumberOfGenes();


  /**
    Return a vector that contains all genes, each gene is 1 if it was inferred, and 0 otherwise
  */
  vector<int> getGeneWasInferred(){
    return this->geneWasInferred;
  }

private:
  /**
		Creates all genes from the dataset
	*/
  void createGenes(ifstream & data);

  /**
		Calculates and store all genes joint entropies on JointEntropiesMatrix
	*/
  void computesJointEntropies();

  /**
		Calculates and store all genes mutual informations on MutualInformationMatrix
	*/
  void computesMutualInformations();

  /**
		Calculates the most informative genes for each gene
	*/
  void runMIFS();

  /**
		Stores the k most informative genes of v0 on MIFSmatrix
	*/
  void MIFS(int v0, int k);

  /**
		Find the first gene that has highest mutual information with v0
	*/
  Gene *findArgmax(vector<Gene *> *W, int v0);

  /**
		Find the remainig genes that has highest mutual information with v0
	*/
  Gene *findArgmaxRepeat(int v0, vector<Gene *> *W, vector<Gene *> S);

  /**
		Tries to improve MIFS result doing a iteratively swapping with the remaining genes
	*/
  void runSWAP();

  /**
		Stores the updated most informative genes (that improves the solution dynamics accuracy) of v0 on SWAPmatrix 
	*/
  void SWAP(int v0, vector<Gene *> S, vector<Gene *> W);

  /**
		Return the gene-wise dynamics consistency calculated after finding the best update rule
	*/
  double searchUpdateRule(vector<int> v0, vector<Gene *> S);

  /**
		Return the update rule which will probably improves the solution
	*/
  double selectBestUpdateRule(vector<int> v0, vector<int> andRule, vector<int> orRule);

  /**
		Stores all possible edges and it's value (represents if a pair of genes is connected) on the inferred network matrix 
	*/
  void fillInferredNetwork();

  /**
		Creates the .dot files the needed information to generate the network images
	*/
  void dotFiles(string test);

  int kValue;
  int numberOfGenes, numberOfSamples, numberOfEdges;
  double **jointEntropyValues;      //matrix with all genes joint entropies
  double **mutualInformationValues; //matrix with all genes mutual informations
  int **MIFSmatrix;                 //store values after mifs search for each S
  int **remainingGenes;             //genes that were not chosen on MIFS
  int **SWAPmatrix;                 //store values after mifs search for each S and swap try to improves the dynamics accuracy
  int **inferredNetwork;            //store all edges values, that represents if a pair of genes is conected in the inferred network or not

  vector<int> geneWasInferred;
  vector<Gene *> genes;

  friend class Gene;
};