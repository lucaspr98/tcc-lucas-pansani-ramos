The ROC results for this dataset are:

TP:			8
FP:			110
Precision:		6.78%
Recall:			3.21%
Structural Accuracy:	96.42%
Similarity:		25.89%
