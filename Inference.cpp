/**
	Inference.cpp

    Purpose: Implements the methods created on Inference.h

    Author: Lucas Pansani Ramos
*/
#include "Inference.h"

void Inference::createGenes(ifstream &data)
{
    int i, j;

    //Creates numberOfGenes genes
    for (i = 0; i < this->numberOfGenes; i++)
    {
        this->genes.push_back(new Gene(i));
    }

    //For each gene, store his sample values
    for (i = 0; i < this->numberOfSamples; i++)
    {
        for (j = 0; j < this->numberOfGenes; j++)
        {
            int val;
            data >> val;
            this->genes[j]->addSampleValues(val);
        }
    }

    //For each gene, computes its entropy value
    for (i = 0; i < this->numberOfGenes; i++)
    {
        this->genes[i]->computesEntropy();
    }
}

void Inference::fillInferredNetwork()
{
    int i;
    int numberOfGenes = this->numberOfGenes;
    this->numberOfEdges = (numberOfGenes * numberOfGenes) - numberOfGenes;
    this->inferredNetwork = new int *[numberOfEdges];
    for (i = 0; i < numberOfEdges; i++)
    {
        this->inferredNetwork[i] = new int[3];
    }

    int inferredRow = 0;
    for (i = 0; i < numberOfGenes; i++)
    {
        auto begin = this->genes[i]->getMostInformativeGenesBegin();
        auto end = this->genes[i]->getMostInformativeGenesEnd();
        for (auto it = begin; it != end; ++it)
        {
            this->inferredNetwork[inferredRow][0] = i + 1;
            this->inferredNetwork[inferredRow][1] = ((*it)->getId()) + 1;
            this->inferredNetwork[inferredRow][2] = 1;
            inferredRow++;
        }
    }

    for (i = 0; i < numberOfGenes; i++)
    {
        auto begin = this->genes[i]->getLessInformativeGenesBegin();
        auto end = this->genes[i]->getLessInformativeGenesEnd();
        for (auto it = begin; it != end - 1; ++it)
        {
            this->inferredNetwork[inferredRow][0] = i + 1;
            this->inferredNetwork[inferredRow][1] = (*it)->getId() + 1;
            this->inferredNetwork[inferredRow][2] = 0;
            inferredRow++;
        }
    }
}

int **Inference::getInferredNetwork()
{
    return this->inferredNetwork;
}

int Inference::getNumberOfEdges()
{
    return this->numberOfEdges;
}

int Inference::getNumberOfGenes()
{
    return this->numberOfGenes;
}

void Inference::computesJointEntropies()
{
    int i, j;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        for (j = 0; j < this->numberOfGenes; j++)
        {
            if (i == j)
                this->jointEntropyValues[i][j] = 0;
            else
                this->jointEntropyValues[i][j] = this->genes[i]->computesJointEntropy(this->genes[j]);
        }
    }
}

void Inference::computesMutualInformations()
{
    int i, j;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        for (j = 0; j < this->numberOfGenes; j++)
        {
            if (i == j)
                this->mutualInformationValues[i][j] = 0;
            else
                this->mutualInformationValues[i][j] = this->genes[i]->computesMutualInformation(this->genes[j], this->jointEntropyValues[i][j]);
        }
    }
}

void Inference::runMIFS()
{
    int i;
    int genesSize = (int)this->genes.size();

    //allocate MIFSmatrix
    this->MIFSmatrix = new int *[this->numberOfGenes];

    for (i = 0; i < this->numberOfGenes; i++)
        this->MIFSmatrix[i] = new int[this->kValue];

    //allocate remainingGenes
    this->remainingGenes = new int *[this->numberOfGenes];

    for (i = 0; i < this->numberOfGenes; i++)
        this->remainingGenes[i] = new int[this->numberOfGenes - this->kValue];

    for (i = 0; i < genesSize; i++)
    {
        MIFS(this->genes[i]->getId(), this->kValue);
    }
}

void Inference::MIFS(int v0, int k)
{
    int i;
    int genesSize = (int)this->genes.size();
    vector<Gene *> W;
    vector<Gene *> S; //S = 0
    //W = {w1,w2,...,wm}
    for (i = 0; i < genesSize; i++)
    {
        W.push_back(genes[i]);
    }

    Gene *aux = findArgmax(&W, v0);

    //S += v
    S.push_back(aux);
    this->MIFSmatrix[v0][0] = aux->getId();

    i = 1;
    k--;
    while (k--)
    {
        aux = findArgmaxRepeat(v0, &W, S);
        S.push_back(aux);
        this->MIFSmatrix[v0][i] = aux->getId();
        i++;
    }

    i = 0;
    for (auto it = W.begin(); it != W.end(); ++it)
    {
        this->remainingGenes[v0][i++] = (*it)->getId();
    }
}

//v = argmax w E W I(v,w)
Gene *Inference::findArgmax(vector<Gene *> *W, int v0)
{
    int i;
    double v = -INFINITY;

    Gene *aux;

    int wSize = (int)W->size();
    int pos = 0;
    for (i = 0; i < wSize; i++)
    {
        if ((*W)[i]->getId() != v0)
        {
            if (this->mutualInformationValues[v0][(*W)[i]->getId()] > v)
            {
                v = this->mutualInformationValues[v0][(*W)[i]->getId()];
                aux = (*W)[i];
                pos = i;
            }
        }
    }

    //W -= aux
    W->erase(W->begin() + pos);

    return aux;
}

Gene *Inference::findArgmaxRepeat(int v0, vector<Gene *> *W, vector<Gene *> S)
{
    int i, j;
    double v = -INFINITY;
    Gene *aux;
    int wSize = (int)W->size();
    int sSize = (int)S.size();
    int pos = 0;
    for (i = 0; i < wSize; i++)
    {
        if ((*W)[i]->getId() != v0)
        {
            double Ssum = 0;
            for (j = 0; j < sSize; j++)
            {
                Ssum += this->mutualInformationValues[(*W)[i]->getId()][S[j]->getId()];
            }
            if ((this->mutualInformationValues[v0][(*W)[i]->getId()] - Ssum) > v)
            {
                v = this->mutualInformationValues[v0][(*W)[i]->getId()];
                aux = (*W)[i];
                pos = i;
            }
        }
    }

    //W -= aux
    W->erase(W->begin() + pos);

    return aux;
}

void Inference::runSWAP()
{
    int i, j;
    vector<Gene *> S;
    vector<Gene *> W;

    //allocate SWAPmatrix
    this->SWAPmatrix = new int *[this->numberOfGenes];
    for (i = 0; i < this->numberOfGenes; i++)
        this->SWAPmatrix[i] = new int[this->kValue];

    for (i = 0; i < this->numberOfGenes; i++)
    {
        //gets v0 S from MIFSmatrix
        for (j = 0; j < this->kValue; j++)
        {
            S.push_back(this->genes[this->MIFSmatrix[i][j]]);
        }
        //gets v0 remaining genes
        for (j = 0; j < (this->numberOfGenes - this->kValue); j++)
        {
            W.push_back(this->genes[this->remainingGenes[i][j]]);
        }

        SWAP(this->genes[i]->getId(), S, W);

        S.clear();
        W.clear();
    }

    double limiting = 0;
    int usedInformations = 0;

    if (this->numberOfGenes == 100)
    {
        for (i = 0; i < numberOfGenes; i++)
        {
            for (j = 0; j < this->kValue; j++)
            {
                if (this->mutualInformationValues[i][this->SWAPmatrix[i][j]] > 0)
                {
                    limiting += this->mutualInformationValues[i][this->SWAPmatrix[i][j]];
                    usedInformations++;
                };
            }
        }
        limiting /= usedInformations;
    }

    for (i = 0; i < this->numberOfGenes; i++)
    {

        for (j = 0; j < this->kValue; j++)
        {
            Gene *g = (this->genes)[this->SWAPmatrix[i][j]];
            if (this->mutualInformationValues[i][g->getId()] > limiting)
            {
                (this->genes[i])->fillMostInformativeGenes(g);
            }
            else
            {
                (this->genes[i])->fillLessInformativeGenes(g);
            }
        }

        (this->genes[i])->sortLessInformativeGenes();
    }
}

void Inference::SWAP(int v0, vector<Gene *> S, vector<Gene *> W)
{
    int i, j;
    vector<int> v0sample;
    double Emax, E, EmaxMI;

    v0sample = this->genes[v0]->getSample();

    sort(W.begin(), W.end(), [&](Gene *a, Gene *b) {
        return mutualInformationValues[v0][a->getId()] > mutualInformationValues[v0][b->getId()];
    });

    Emax = searchUpdateRule(v0sample, S);
    EmaxMI = -INFINITY;

    for (i = 0; i < int(W.size()); i++)
    {
        if (W[i]->getId() != v0)
        {
            int SEmaxPos = 0;
            double SEmax = -INFINITY;
            double SEmaxMI = -INFINITY;
            for (j = 0; j < int(S.size()); j++)
            {
                vector<Gene *> auxS = S;
                vector<Gene *> auxW = W;

                auxS[j] = auxW[i];

                E = searchUpdateRule(v0sample, auxS);

                if (E > SEmax)
                {
                    SEmaxPos = j;
                    SEmax = E;
                    SEmaxMI = this->mutualInformationValues[W[i]->getId()][S[j]->getId()];
                }
                else if (E == SEmax)
                {
                    if (this->mutualInformationValues[W[i]->getId()][S[j]->getId()] > SEmaxMI)
                    {
                        SEmaxPos = j;
                        SEmax = E;
                        SEmaxMI = this->mutualInformationValues[W[i]->getId()][S[j]->getId()];
                    }
                }
            }
            if (SEmax > Emax)
            {
                Gene *aux = S[SEmaxPos];
                S[SEmaxPos] = W[i];
                W[i] = aux;

                Emax = SEmax;
                EmaxMI = this->mutualInformationValues[W[i]->getId()][S[SEmaxPos]->getId()];
            }
            else if (SEmax == Emax)
            {
                if (SEmaxMI > EmaxMI)
                {
                    Gene *aux = S[SEmaxPos];
                    S[SEmaxPos] = W[i];
                    W[i] = aux;

                    Emax = SEmax;
                    EmaxMI = this->mutualInformationValues[W[i]->getId()][S[SEmaxPos]->getId()];
                }
            }
        }
    }

    for (i = 0; i < int(S.size()); i++)
    {
        this->SWAPmatrix[v0][i] = S[i]->getId();
    }

    for (i = 0; i < int(W.size()); i++)
    {
        (this->genes[v0])->fillLessInformativeGenes(W[i]);
    }
}

double Inference::searchUpdateRule(vector<int> v0, vector<Gene *> S)
{
    vector<int> andRule;
    vector<int> orRule;
    int opTrue;

    for (int i = 0; i < int(v0.size()); i++)
    {
        opTrue = 1;
        for (int j = 0; j < int(S.size()); j++)
        {
            opTrue = opTrue & S[j]->getSample()[i];
        }
        andRule.push_back(opTrue);
    }

    for (int i = 0; i < int(v0.size()); i++)
    {
        opTrue = 0;
        for (int j = 0; j < int(S.size()); j++)
        {
            opTrue = opTrue | S[j]->getSample()[i];
        }
        orRule.push_back(opTrue);
    }

    return selectBestUpdateRule(v0, andRule, orRule);
}

double Inference::selectBestUpdateRule(vector<int> v0, vector<int> andRule, vector<int> orRule)
{
    double orE, andE;

    orE = 0;
    andE = 0;

    for (int i = 1; i < (int)(v0.size()); i++)
    {
        if (andRule[i] == v0[i])
            andE++;
        if (orRule[i] == v0[i])
            orE++;
    }

    orE /= (double)(v0.size() - 1.0);
    andE /= (double)(v0.size() - 1.0);

    if (orE > andE)
    {
        return orE;
    }
    else
    {
        return andE;
    }
}

void Inference::printJointEntropies()
{
    int i, j;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        for (j = 0; j < this->numberOfGenes; j++)
        {
            cout << this->jointEntropyValues[i][j] << "\t\t\t";
        }
        cout << endl;
    }
}

void Inference::printMutualInformations()
{
    int i, j;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        for (j = 0; j < this->numberOfGenes; j++)
            cout << this->mutualInformationValues[i][j] << "\t\t\t\t";
        cout << endl;
    }
}

void Inference::printMIFSmatrix()
{
    int i, j;
    cout << "MIFS Matrix:" << endl;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        for (j = 0; j < this->kValue; j++)
        {
            cout << this->MIFSmatrix[i][j] << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

void Inference::printSWAPmatrix()
{
    int i, j;
    cout << "SWAP Matrix:" << endl;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        for (j = 0; j < this->kValue; j++)
        {
            cout << this->SWAPmatrix[i][j] << "\t";
        }
        cout << endl;
    }
    cout << endl;
}

void Inference::printMostInformativeMI()
{
    for (int i = 0; i < this->numberOfGenes; i++)
    {
        auto begin = (this->genes[i])->getMostInformativeGenesBegin();
        auto end = (this->genes[i])->getMostInformativeGenesEnd();
        for (auto it = begin; it != end; ++it)
        {
            cout << this->mutualInformationValues[i][(*it)->getId()] << "\t";
        }
        cout << endl;
    }
}

void Inference::printRemainingGenes()
{
    int i, j;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        for (j = 0; j < (this->numberOfGenes - this->kValue); j++)
        {
            cout << this->remainingGenes[i][j] << "\t\t";
        }
        cout << endl;
    }
}

void Inference::dotFiles(string test)
{
    int i, j;
    string dot = ".dot";
    string mifsFile = "dotFiles/mifs_";
    string swapFile = "dotFiles/swap_";

    ofstream mifsDot(mifsFile + test + dot);

    mifsDot << "digraph G{\n";
    mifsDot << "\tgraph [ratio=\"compress\", size=\"10,10\"];\n";
    mifsDot << "\tedge [arrowhead=\"open\"];\n";
    for (i = 0; i < numberOfGenes; i++)
    {
        mifsDot << "\t" << i << "[label=\"G" << i << "\"];\n";
    }
    mifsDot << endl;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        for (j = 0; j < this->kValue; j++)
        {
            mifsDot << "\t" << i << " -> " << this->MIFSmatrix[i][j] << "[color=\"blue\"];\n";
        }
    }
    mifsDot << "}";

    ofstream swapDot(swapFile + test + dot);

    swapDot << "digraph G{\n";
    swapDot << "\tgraph [ratio=\"compress\", size=\"10,10\"];\n";
    swapDot << "\tedge [arrowhead=\"open\"];\n";
    for (i = 0; i < numberOfGenes; i++)
    {
        swapDot << "\t" << i << "[label=\"G" << i + 1 << "\"];\n";
    }
    swapDot << endl;
    for (i = 0; i < this->numberOfGenes; i++)
    {
        auto begin = this->genes[i]->getMostInformativeGenesBegin();
        auto end = this->genes[i]->getMostInformativeGenesEnd();
        for (auto it = begin; it != end; ++it)
            swapDot << "\t" << i << " -> " << (*it)->getId() << "[color=\"blue\"];\n";
    }
    swapDot << "}";
}