/**
	Gene.h

    Purpose: Represents a gene. It has sample values that are used to calculate it's entropy. It also has two lists that store the most and less informative genes for it. 

    Author: Lucas Pansani Ramos
*/
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

class Gene {
	public:
		/**
			Initialize this gene
		*/
		Gene(int id) {
			this->gene_id = id;
			this->positive = 0;
			this->negative = 0;
		}

		/**
			Receives the value of this gene at a time serie and store it on it sample values list
		*/
		void addSampleValues(int val);

		/**
			Return this gene entropy
		*/
		void computesEntropy();

		/**
			Return the joint entropy of this gene with another gene y
		*/
		double computesJointEntropy(Gene *y);		

		/**
			Return the mutual information of this gene with another gene y
		*/
		double computesMutualInformation(Gene *y, double xyJE);

		/**
			Stores a gene y in this gene mostInformativeGenes vector
		*/
		void fillMostInformativeGenes(Gene *y);

		/**
			Stores a gene y in this gene lessInformativeGenes vector
		*/
		void fillLessInformativeGenes(Gene *y);

		/**
			Sort this gene lessInformativeGenes vector by gene_id
		*/
		void sortLessInformativeGenes();

		/**
			Return this gene mostInformativeGenes begin iterator
		*/
		vector<Gene*>::iterator getMostInformativeGenesBegin();

		/**
			Return this gene mostInformativeGenes end iterator
		*/
		vector<Gene*>::iterator getMostInformativeGenesEnd();

		/**
			Return this gene lessInformativeGenes begin iterator
		*/
		vector<Gene*>::iterator getLessInformativeGenesBegin();

		/**
			Return this gene lessInformativeGenes end iterator
		*/
		vector<Gene*>::iterator getLessInformativeGenesEnd();

		/**
			Return this gene_id
		*/
		int getId();

		/**
			Return this gene sample
		*/
		vector<int> getSample();

		/**
			Return this gene sample size
		*/
		int sampleSize();

		/**
			Return this gene entropy
		*/
		double getEntropy();

		/**
			Return the amount of positive values on this gene sample 
		*/
		int getPositive();

		/**
			Return the amount of negative values on this gene sample 
		*/
		int getNegative();
		
		/**
			Prints this gene sample values
		*/
		void printSample();

		/**
		    Returns if mostInformativeGenes is empty or not
		*/
		bool mostInformativeGenesSize(){
			return this->mostInformativeGenes.size();
		}

	private:
		vector<int> sample;
		vector<Gene*> mostInformativeGenes;
		vector<Gene*> lessInformativeGenes;
		int gene_id;
		int positive, negative;
		double entropy;

};