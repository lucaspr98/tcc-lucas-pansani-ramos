The ROC results for this dataset are:

TP:			5
FP:			25
Precision:		16.67%
Recall:			31.25%
Structural Accuracy:	55.56%
Similarity:		32.03%
