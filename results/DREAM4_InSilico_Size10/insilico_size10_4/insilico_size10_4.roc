The ROC results for this dataset are:

TP:			5
FP:			25
Precision:		16.67%
Recall:			38.46%
Structural Accuracy:	59.76%
Similarity:		32.60%
