/**
	Validation.h

    Purpose: Validates the inferred network comparing it with the real network

    Author: Lucas Pansani Ramos
*/
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <vector>
#include <cmath>

using namespace std;

class Validation
{
public:
  Validation(ifstream & realNetwork, int **inferredNetwork, int numberOfEdges, int numberOfGenes, vector<int> geneWasInferred, string fileName)
  {
    this->numberOfEdges = numberOfEdges;
    this->geneWasInferred = geneWasInferred;

    this->initializeNetworks(realNetwork, inferredNetwork);

    string dotFile = fileName;
    dotFile.append(".dot");

    ofstream finalNetworkDot(dotFile);

    this->printDot(finalNetworkDot, numberOfGenes);

    this->printRocResults(fileName);
  
  };

private:
  /**
		Initializes this realNetwork and inferredNetwork matrix with the passed parameters
	*/
  void initializeNetworks(ifstream & realNetwork, int **inferredNetwork);

  /**
		Computes all ROC metrics
	*/
  void computesAllMeasures(ofstream & dotFile);

  /**
		Computes the amount of edges that are in the real network and are in the inferred network
	*/
  void computesTP(ofstream & dotFile);

  /**
		Computes the amount of edges that are not in the real network and are not in the inferred network
	*/
  void computesTN();

  /**
		Computes the amount of edges that are not in the real network and are in the inferred network
	*/
  void computesFP(ofstream & dotFile);

  /**
		Computes the amount of edges that are in the real network and are not in the inferred network
	*/
  void computesFN();

  /**
		Computes the percentage of correctly inferred connections out of all predictions
	*/
  void computesPrecision();

  /**
		Computes the percentage of inferred connections among the true connections in the real network
	*/
  void computesRecall();

  /**
		Computes the percentage of correct predictions
	*/
  void computesStructuralAccuracy();

  void computesSimilarity();

  /**
		Prints the .dot of the inferred network coloring the correct and incorrect predictions
	*/
  void printDot(ofstream & dotFile, int numberOfGenes);

  /**
		Prints the ROC metrics results on a file
	*/
  void printRocResults(string fileName);

  int **realNetwork;
  int **inferredNetwork;
  int TP, TN, FP, FN;
  int numberOfEdges;
  double precision, recall, structuralAccuracy, similarity;
  vector<int> geneWasInferred;
};