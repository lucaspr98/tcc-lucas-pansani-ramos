The ROC results for this dataset are:

TP:			5
FP:			22
Precision:		18.52%
Recall:			33.33%
Structural Accuracy:	60.98%
Similarity:		35.27%
