The ROC results for this dataset are:

TP:			2
FP:			20
Precision:		9.09%
Recall:			14.29%
Structural Accuracy:	60.49%
Similarity:		25.25%
