/**
	Gene.cpp

    Purpose: Implements the methods created on Gene.h

    Author: Lucas Pansani Ramos
*/
#include "Gene.h"

void Gene::addSampleValues(int val)
{
	this->sample.push_back(val);
	if (val == 0)
		this->positive++;
	else
		this->negative++;
}

void Gene::computesEntropy()
{
	int sz = (this->sample).size();
	double px0 = (double)this->negative / sz;
	double px1 = (double)this->positive / sz;
	if (this->negative == sz || this->positive == sz)
		this->entropy = 0;
	else
		this->entropy = (-1) * (px1 * log2(px1) + px0 * log2(px0));
}

double Gene::computesJointEntropy(Gene *y)
{
	int sz = (this->sample).size();
	double pospos, posneg, negpos, negneg;
	double xy00, xy01, xy10, xy11;
	xy00 = xy01 = xy10 = xy11 = 0;

	for (int i = 0; i < sz; i++)
	{
		if (this->sample[i] == 0 && y->sample[i] == 0)
		{
			xy00++;
		}
		else if (this->sample[i] == 0 && y->sample[i] == 1)
		{
			xy01++;
		}
		else if (this->sample[i] == 1 && y->sample[i] == 0)
		{
			xy10++;
		}
		else
		{
			xy11++;
		}
	}

	double mat[2][2] = {{xy00 / sz, xy01 / sz},
						{xy10 / sz, xy11 / sz}};

	if (mat[0][0] == 0)
	{
		negneg = 0;
	}
	else
	{
		negneg = mat[0][0] * log2(mat[0][0]);
	}
	if (mat[0][1] == 0)
	{
		negpos = 0;
	}
	else
	{
		negpos = mat[0][1] * log2(mat[0][1]);
	}
	if (mat[1][0] == 0)
	{
		posneg = 0;
	}
	else
	{
		posneg = mat[1][0] * log2(mat[1][0]);
	}
	if (mat[1][1] == 0)
	{
		pospos = 0;
	}
	else
	{
		pospos = mat[1][1] * log2(mat[1][1]);
	}

	double JE = (-1) * (pospos + posneg + negpos + negneg);
	return JE;
}

double Gene::computesMutualInformation(Gene *y, double xyJE)
{
	double MI = this->entropy + y->getEntropy() - xyJE;
	return MI;
}

void Gene::fillMostInformativeGenes(Gene *y)
{
	this->mostInformativeGenes.push_back(y);
}

vector<Gene *>::iterator Gene::getMostInformativeGenesBegin()
{
	return this->mostInformativeGenes.begin();
}

vector<Gene *>::iterator Gene::getMostInformativeGenesEnd()
{
	return this->mostInformativeGenes.end();
}

void Gene::fillLessInformativeGenes(Gene *y)
{
	this->lessInformativeGenes.push_back(y);
}

bool compare(Gene *a, Gene *b)
{
	return a->getId() < b->getId();
}

void Gene::sortLessInformativeGenes()
{
	sort(this->lessInformativeGenes.begin(), this->lessInformativeGenes.end(), compare);
}

vector<Gene *>::iterator Gene::getLessInformativeGenesBegin()
{
	return this->lessInformativeGenes.begin();
}

vector<Gene *>::iterator Gene::getLessInformativeGenesEnd()
{
	return this->lessInformativeGenes.end();
}

int Gene::getId()
{
	return this->gene_id;
}

double Gene::getEntropy()
{
	return this->entropy;
}

vector<int> Gene::getSample()
{
	return this->sample;
}

int Gene::sampleSize()
{
	return this->sample.size();
}

int Gene::getPositive()
{
	return this->positive;
}

int Gene::getNegative()
{
	return this->negative;
}

void Gene::printSample()
{
	int sz = this->sample.size();
	for (int i = 0; i < sz; i++)
		cout << this->sample[i] << " ";
	cout << endl;
}