#tcc makefile

all: tcc

tcc: main.o gene.o inference.o validation.o
	g++ -o tcc main.o gene.o inference.o validation.o

main.o: main.cpp Inference.h
	g++ -o main.o main.cpp -c -W -Wall -ansi -pedantic -std=c++14

inference.o: Inference.cpp Inference.h
	g++ -o inference.o Inference.cpp -c -W -Wall -ansi -pedantic -std=c++14

gene.o: Gene.cpp Gene.h
	g++ -o gene.o Gene.cpp -c -W -Wall -ansi -pedantic -std=c++14

validation.o: Validation.cpp Validation.h
	g++ -o validation.o Validation.cpp -c -W -Wall -ansi -pedantic -std=c++14

clean:
	rm -rf *.o *~ tcc