The ROC results for this dataset are:

TP:			8
FP:			107
Precision:		6.96%
Recall:			4.17%
Structural Accuracy:	97.03%
Similarity:		26.23%
