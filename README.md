# Inference algorithms of gene regulatory networks
## Author: Lucas Pansani Ramos

In the project directory use the next command to compile the program:

### `make`

after that, you can run the program passing the following parameters

### `./tcc <dataset-path> <dataset-file> <dataset-goldstandard-file> <k>`

Here is an example:

### `./tcc data/DREAM4_InSilico_Size10/insilico_size10_1 insilico_size10_1.txt DREAM4_GoldStandard_InSilico_Size10_1.tsv 3`

In this example we will run an inference algorithm on dataset **insilico_size10_1.txt** trying to find the **k** most informative genes for each gene. And then, compare the result with **DREAM4_GoldStandard_InSilico_Size10_1.tsv**.

The inference algorithm implemented is based on the article "A novel mutual information-based Boolean network inference method from time-series gene expression data". That can be found at  <https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0171097>.