/**
	Validation.cpp

    Purpose: Implements the methods created on Validation.h

    Author: Lucas Pansani Ramos
*/
#include "Validation.h"

void Validation::initializeNetworks(ifstream &realNetwork, int **inferredNetwork)
{
    int i;
    this->realNetwork = new int *[numberOfEdges];
    this->inferredNetwork = new int *[numberOfEdges];
    for (i = 0; i < numberOfEdges; i++)
    {
        this->realNetwork[i] = new int[3];
        this->inferredNetwork[i] = new int[3];
    }

    for (i = 0; i < this->numberOfEdges; i++)
    {
        int geneA, geneB, value;
        realNetwork >> geneA >> geneB >> value;
        this->realNetwork[i][0] = geneA;
        this->realNetwork[i][1] = geneB;
        this->realNetwork[i][2] = value;

        this->inferredNetwork[i][0] = inferredNetwork[i][0];
        this->inferredNetwork[i][1] = inferredNetwork[i][1];
        this->inferredNetwork[i][2] = inferredNetwork[i][2];
    }
}

void Validation::computesAllMeasures(ofstream &finalNetworkDot)
{
    this->computesTP(finalNetworkDot);
    this->computesFP(finalNetworkDot);
    this->computesFN();
    this->computesTN();
    this->computesPrecision();
    this->computesRecall();
    this->computesStructuralAccuracy();
    this->computesSimilarity();
}

void Validation::computesPrecision()
{
    this->precision = ((double)this->TP / (double)(this->TP + this->FP));
}

void Validation::computesRecall()
{
    this->recall = ((double)this->TP / ((double)this->TP + this->FN));
}

void Validation::computesStructuralAccuracy()
{
    this->structuralAccuracy = ((double)(this->TP + this->TN) / (double)(this->TP + this->FP + this->FN + this->TN));
}

void Validation::computesSimilarity()
{
    double specificity = (double)(this->TN)/double(this->TN + this->FP); 
    this->similarity = sqrt(this->precision*specificity);
}

void Validation::computesTP(ofstream &finalNetworkDot)
{
    int i, j;
    this->TP = 0;
    for (i = 0; i < numberOfEdges; i++)
    {
        for (j = 0; j < numberOfEdges; j++)
        {
            if (this->inferredNetwork[i][2] == 1)
            {
                if (this->inferredNetwork[i][0] == this->realNetwork[j][0] && this->inferredNetwork[i][1] == this->realNetwork[j][1] && this->inferredNetwork[i][2] == this->realNetwork[j][2])
                {
                    this->TP++;
                    finalNetworkDot << "\t" << this->inferredNetwork[i][0] - 1 << " -> " << this->inferredNetwork[i][1] - 1 << "[color=\"#00cc00\"];\n";
                    break;
                }
            }
        }
    }
}

void Validation::computesTN()
{
    int i, j;
    this->TN = 0;
    for (i = 0; i < numberOfEdges; i++)
    {
        for (j = 0; j < numberOfEdges; j++)
        {
            if (this->inferredNetwork[i][2] == 0)
            {
                if (this->inferredNetwork[i][0] == this->realNetwork[j][0] && this->inferredNetwork[i][1] == this->realNetwork[j][1] && this->inferredNetwork[i][2] == this->realNetwork[j][2])
                {
                    this->TN++;
                    break;
                }
            }
        }
    }
    cout << TP << endl;
    cout << TN << endl;

    cout << FP << endl;

    cout << FN << endl;

}

void Validation::computesFP(ofstream &finalNetworkDot)
{
    int i, j;
    this->FP = 0;
    for (i = 0; i < numberOfEdges; i++)
    {
        for (j = 0; j < numberOfEdges; j++)
        {
            if (this->inferredNetwork[i][2] == 1)
            {
                if (this->inferredNetwork[i][0] == this->realNetwork[j][0] && this->inferredNetwork[i][1] == this->realNetwork[j][1] && this->inferredNetwork[i][2] != this->realNetwork[j][2])
                {
                    this->FP++;
                    finalNetworkDot << "\t" << this->inferredNetwork[i][0] - 1 << " -> " << this->inferredNetwork[i][1] - 1 << "[color=\"#e60000\"];\n";
                    break;
                }
            }
        }
    }
}

void Validation::computesFN()
{
    int i, j;
    this->FN = 0;
    for (i = 0; i < numberOfEdges; i++)
    {
        for (j = 0; j < numberOfEdges; j++)
        {
            if (this->inferredNetwork[i][2] == 0)
            {
                if (this->inferredNetwork[i][0] == this->realNetwork[j][0] && this->inferredNetwork[i][1] == this->realNetwork[j][1] && this->inferredNetwork[i][2] != this->realNetwork[j][2])
                {
                    this->FN++;
                    break;
                }
            }
        }
    }
}

void Validation::printRocResults(string fileName)
{
    string file = fileName;
    file.append(".roc");

    ofstream roc(file);

    roc.precision(2);
    roc << "The ROC results for this dataset are:\n\n";
    roc << "TP:\t\t\t" << this->TP << endl;
    roc << "FP:\t\t\t" << this->FP << endl;
    roc << "Precision:\t\t" << fixed << this->precision * 100 << "%\n";
    roc << "Recall:\t\t\t" << fixed << this->recall * 100 << "%\n";
    roc << "Structural Accuracy:\t" << fixed << this->structuralAccuracy * 100 << "%\n";
    roc << "Similarity:\t\t" << fixed << this->similarity * 100 << "%\n";
}

void Validation::printDot(ofstream &finalNetworkDot, int numberOfGenes)
{

    finalNetworkDot << "digraph G{\n";
    finalNetworkDot << "\tgraph [ratio=\"compress\", size=\"100,100\"];\n";
    finalNetworkDot << "\tedge [arrowhead=\"open\"];\n";

    for (int i = 0; i < numberOfGenes; i++)
    {   
        if(this->geneWasInferred[i])
            finalNetworkDot << "\t" << i << "[label=\"G" << i << "\"];\n";            
    }
    

    computesAllMeasures(finalNetworkDot);

    finalNetworkDot << "}";
}